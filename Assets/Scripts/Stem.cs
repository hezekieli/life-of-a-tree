using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stem : MonoBehaviour {
	
	public bool ShootFromRoot;
	public Stem Parent;
	public Root ParentRoot;
	public GameObject SpriteGO;

	public bool GrowsLength = true;
	public float StemLength = 0.02f;	// Stem-root is at (-0.5f * StemLength, 0f) and tip at (0.5f * StemLength, 0f)
	public float StemWidth = 0.01f;

	float RestAngle;

	float NaturalRotation = 0f;
	int BudCount = 0;
	int BudsMax = 1;
	float BudDelay = 10f;
	public float BudProgress = 2f;
	public static float sizeToWeight = 1000f;		// StemLenght * StemWidth = size

	Rigidbody2D stemRigidbody2D;
	FixedJoint2D stemFixedJoint2D;
	int PhysicsDelay = 2;
	int PhysicsProgress = 0;

	// Length is transform.localScale.y.
	// Width is transform.localScale.x.
	// Weight goes to Rigidbody2d and is calculated Length * Width * sizeToWeight
	
	List<Bud> Buds = new List<Bud>();
	List<Stem> Shoots = new List<Stem>();
	List<Leaf> Leaves = new List<Leaf>();

	// Use this for initialization
	void Start () {

		//StemLength = 0.02f;		// Starts at 2cm
		//StemWidth = 0.01f;		// Starts at 1cm

		stemRigidbody2D = gameObject.GetComponent<Rigidbody2D> ();
		stemFixedJoint2D = gameObject.GetComponent<FixedJoint2D> ();

		ScalePhysics ();
		ScaleSprites ();

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (BudCount < BudsMax) {
			if (BudProgress <= 0f )
			{
				/*
				Buds.Add (new Vector2 (0f, 0.1f));
				Buds.Add (new Vector2 (0f, 0.05f));
				BudProgress = BudDelay;
				BudCount += 2;
				*/

				GrowsLength = false;

				// Random.value * 0.8f * StemLength - 0.4f * StemLength
				GameObject budGO = Instantiate (Resources.Load ("Bud"), this.transform.TransformPoint(Random.value * StemLength * 0.5f, 0f, 0f), Quaternion.identity) as GameObject;
				budGO.transform.parent = this.transform;
				budGO.transform.localEulerAngles = new Vector3(0f, 0f, Random.value * 90f - 45f);
				BudProgress = BudDelay;
				BudCount++;
			}
			else 
			{
				BudProgress -= Time.deltaTime;

			}
		}

		if (PhysicsProgress <= 0) {
			if (GrowsLength)
				GrowLengthAndWidth ();
			else
				GrowWidth ();
			PhysicsProgress = PhysicsDelay;
		} else
			PhysicsProgress--;

		// Grow the stem
			// Set weight
			// Set Joint base strength
		// Check angle compared to limits.
		// Set Joint Motor strength
		// Scale Sprites according to size

		//if (transform.localEulerAngles.z 
	}

	Vector3 GetStemRoot() 
	{
		return transform.TransformPoint (-0.5f * StemLength, 0f, 0f);
	}

	Vector3 GetStemRoot(Stem stem) 
	{
		return new Vector3 ();
	}

	Vector3 GetStemTip()
	{
		return transform.TransformPoint (0.5f * StemLength, 0f, 0f);
	}

	Vector3 GetStemTip(Stem stem)
	{
		return new Vector3 ();
	}

	/** Stem length and width grow based on species growth rate, stem state, available resources in stem
	 * 	RigidBody2D weight and HingeJoint2D motor force are set based on stem length and width
	 * 	Lastly, the stem visuals are set based on stem stats.
	 */
	void GrowLengthAndWidth()
	{
		StemLength += 0.05f * Time.deltaTime;
		StemWidth += 0.001f * Time.deltaTime;

		ScalePhysics ();
		ScaleSprites ();
	}

	void GrowWidth()
	{
		StemWidth += 0.002f * Time.deltaTime;

		ScalePhysics ();
		ScaleSprites ();
	}

	void ScalePhysics() 
	{
		//JointMotor2D motor = stemHingeJoint2D.motor;
		//motor.maxMotorTorque = StemWidth * widthToTorque;
		//stemHingeJoint2D.motor = motor;

		if (stemFixedJoint2D) stemFixedJoint2D.anchor = new Vector2(-0.5f * StemLength, 0f);

		stemRigidbody2D.mass = StemLength * StemWidth * StemWidth * sizeToWeight;
	}

	void ScaleSprites() 
	{
		SpriteGO.transform.localScale = new Vector3 (0.9f * StemLength, 2f * StemWidth);
	}

	public void ShootStem (Bud bud) 
	{
		Debug.Log(GetStemTip ().x);

		//Vector2 budPosition = new Vector2 (bud.transform.position.x, bud.transform.position.y);
		//float shootAngle = Mathf.Deg2Rad * transform.localEulerAngles.z + Random.value * Mathf.PI/2 - Mathf.PI/4;
		GameObject stemGO = Instantiate (Resources.Load ("Stem"), new Vector3(bud.transform.position.x, bud.transform.position.y, 0f), Quaternion.identity) as GameObject;
		Stem stem = stemGO.GetComponent<Stem>();

		//stemGO.transform.localEulerAngles = new Vector3(0f, 0f, shootAngle*Mathf.Rad2Deg);
		stem.transform.eulerAngles = bud.transform.eulerAngles;
		stem.transform.position = stem.transform.TransformPoint(0.5f * stem.StemLength, 0f, 0f);

		stem.Parent = this;	// Not transform.parent!


		//Rigidbody2D rb2d = stemGO.GetComponent<Rigidbody2D>();
		FixedJoint2D fj2d = stem.GetComponent<FixedJoint2D>();
		fj2d.anchor = new Vector2(-0.5f * stem.StemLength, 0f);
		fj2d.connectedAnchor = bud.transform.localPosition;
		fj2d.connectedBody = this.GetComponent<Rigidbody2D>();
		fj2d.enabled = true;

		Destroy(bud.gameObject);
		BudCount--;
		//Buds.Remove(Buds[0]);
		//Debug.Log ("Buds.Count After = " + Buds.Count);


	}

	/*
	public void ShootStem () {
		//Debug.Log ("Buds Count Before = " + Buds.Count);
		if (Buds.Count > 0) 
		{
			GameObject stemGO = Instantiate (Resources.Load ("Stem"), new Vector3(Buds[0].x, Buds[0].y, 0f), Quaternion.identity) as GameObject;
			stemGO.transform.localEulerAngles = new Vector3(0f, 0f, Random.value * 90f - 45f);
			Stem stem = stemGO.GetComponent<Stem>();
			//Rigidbody2D rb2d = stemGO.GetComponent<Rigidbody2D>();
			HingeJoint2D hj2d = stemGO.GetComponent<HingeJoint2D>();
			hj2d.anchor = new Vector2(0f, 0f);
			hj2d.connectedAnchor = new Vector2(Buds[0].x, Buds[0].y);
			hj2d.connectedBody = this.GetComponent<Rigidbody2D>();
			Buds.Remove(Buds[0]);
			//Debug.Log ("Buds.Count After = " + Buds.Count);
		}

	}
	*/
}
