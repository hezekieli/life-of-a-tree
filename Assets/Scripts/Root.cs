﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Root : MonoBehaviour {

	public bool AdventitiousBuds = false;

	public bool RootFromSeed;
	public Root parent;
	public Seed parentSeed;

	List<Root> Roots = new List<Root>();
	List<Stem> Shoots = new List<Stem>();

	// Use this for initialization
	void Start () {
	
		// First root from seed needs to be kinematic. It gets heavy, but seed won't.
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShootRoot() {

	}

	/** 
	 * Instantiates a stem-prefab from the seed. Can only shoot once. */
	public void ShootStem () {
		Stem stem = Instantiate (Resources.Load ("Stem") as Stem);
		//stem.AddComponent.<Rigidbody>();
	}
}
