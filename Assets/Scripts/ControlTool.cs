﻿using UnityEngine;
using System.Collections;

public enum Tool
{
	None = 0,
	Water = 1,
	Dry = 2,
	Abscise = 3,
	Leaf = 4,
	Stem = 5,
	Root = 6
}

public class ControlTool : MonoBehaviour {
	
	public Tools ToolSet;
	public bool ToolActive;
	public bool ToolEnabled;
	public Tool CurrentTool = Tool.None;
	public float RangeCurrent = 0.05f;
	float RangeMax = 0.5f;
	float RangeMin = 0.02f;

	Tool mDown;

	//GameObject reticle;
	SpriteRenderer reticleSprite;

	// Use this for initialization
	void Start () {
		//reticle = this.transform.GetChild (0).GetComponent<GameObject>();
		reticleSprite = this.GetComponentInChildren<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {

		Vector2 toolPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		this.transform.position = new Vector3(toolPosition.x, toolPosition.y, 0f);
		
		if (!ToolActive) 
		{
			Collider2D[] toolButtons = Physics2D.OverlapPointAll(toolPosition, 1 << LayerMask.NameToLayer("UI"));

			if (toolButtons.Length > 0) {
				ToolEnabled = false;

				if (Input.GetMouseButtonDown(0))
				{
					switch (toolButtons[0].gameObject.name) 
					{
						case "Tool Water": mDown = Tool.Water;
						break;
						case "Tool Dry": mDown = Tool.Dry;
						break;
						case "Tool Abscise": mDown = Tool.Abscise;
						break;
						case "Tool Leaf": mDown = Tool.Leaf;
						break;
						case "Tool Stem": mDown = Tool.Stem;
						break;
						case "Tool Root": mDown = Tool.Root;
						break;
						default: mDown = Tool.None;
						break;
					}
					Debug.Log ("mDown Tool = " + mDown);
				}
				else if (Input.GetMouseButtonUp(0) && mDown != Tool.None) 
				{
					Tool mUp;

					switch (toolButtons[0].gameObject.name) 
					{
						case "Tool Water": mUp = Tool.Water;
						break;
						case "Tool Dry": mUp = Tool.Dry;
						break;
						case "Tool Abscise": mUp = Tool.Abscise;
						break;
						case "Tool Leaf": mUp = Tool.Leaf;
						break;
						case "Tool Stem": mUp = Tool.Stem;
						break;
						case "Tool Root": mUp = Tool.Root;
						break;
						default: mUp = Tool.None;
						break;
					}
					if (mDown == mUp) CurrentTool = mUp;
				}

			}
			// Mouse not on Buttons
			else 
			{
				ToolEnabled = (CurrentTool != Tool.None) ? true : false;

				if (Input.GetMouseButtonDown(0))
				{
					if (CurrentTool != Tool.None) ToolActive = true;
					//Debug.Log("Tool Activated");
					//Debug.Log("Current Tool: " + CurrentTool);
					// Find colliders within range
				}
			}
		}
		// Tool active
		else 
		{
			if (Input.GetMouseButtonUp(0)) 
			{
				//Debug.Log("Tool Deactivated");
				if (ToolActive) ToolActive = false;

			}
			// If Tool Selected
			else if (CurrentTool != Tool.None)
			{
				if (CurrentTool == Tool.Stem) 
				{
					Collider2D[] buds = Physics2D.OverlapCircleAll(toolPosition, RangeCurrent, 1 << LayerMask.NameToLayer("Buds"));
					if (buds.Length > 0)
					{
						Bud bud = buds[0].GetComponent<Bud>();
						//buds[0].gameObject.GetComponent<Stem>().ShootStem ();
						Debug.Log("Bud.parent = " + bud.transform.parent);
						if (bud.transform.parent == null)
						{
							if (bud.transform.localScale.x < 1.5f) bud.transform.localScale = 1.05f * bud.transform.localScale;
						}
						if (bud.transform.parent != null) bud.transform.parent.gameObject.GetComponent<Stem>().ShootStem(bud.gameObject.GetComponent<Bud>());
					}
				}
				
			}
		}

		// Jos nappi nostetaan
			// 

		// Työkalu valitaan paletista, jos nappi painetaan ja päästetään kun hiiri on työkalunapin päällä


		if (Input.GetKey(KeyCode.LeftControl)) 
		{
			if (Input.GetAxis ("Mouse ScrollWheel") > 0f)
			{
				ScaleTool (true);
			}
			else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
			{
				ScaleTool (false);
			}
		}

		SetToolVisuals();
	}


	void SetToolVisuals()
	{
		float opacity = (ToolEnabled) ? 0.4f : 0f;
		if (!ToolActive) reticleSprite.color = new Color(1f, 1f, 1f, opacity);
		else 
		{
			switch (CurrentTool) 
			{
			case Tool.Water: reticleSprite.color = new Color(0.2f, 0.2f, 1f, opacity);
				break;
			case Tool.Dry: reticleSprite.color = new Color(0.4f, 0.4f, 0.4f, opacity);
				break;
			case Tool.Abscise: reticleSprite.color = new Color(1f, 1f, 0f, opacity);
				break;
			case Tool.Leaf: reticleSprite.color = new Color(0f, 1f, 0f, opacity);
				break;
			case Tool.Stem: reticleSprite.color = new Color(0.5f, 0.8f, 0f, opacity);
				break;
			case Tool.Root: reticleSprite.color = new Color(0.8f, 0.5f, 0f, opacity);
				break;
			}
		}

	}

	void ShowTool() 
	{
		reticleSprite.color = new Color (1f, 1f, 1f, 0.4f);
	}

	void HideTool()
	{
		reticleSprite.color = new Color (1f, 1f, 1f, 0.0f);
	}

	void ActivateTool ()
	{
		ToolActive = true;
		reticleSprite.color = new Color (0.5f, 1f, 0.6f, 0.4f);
	}

	void DeactivateTool ()
	{
		ToolActive = true;
		reticleSprite.color = new Color (1f, 1f, 1f, 0.4f);
	}

	void ScaleTool (bool up)
	{
		if (up) 
		{
			RangeCurrent += 0.02f; 
			RangeCurrent = (RangeCurrent > RangeMax) ? RangeMax: RangeCurrent;
		}
		else 
		{
			RangeCurrent -= 0.02f; 
			RangeCurrent = (RangeCurrent < RangeMin) ? RangeMin: RangeCurrent;
		}
		this.transform.localScale = new Vector3 (RangeCurrent, RangeCurrent, 1f);
	}
}
