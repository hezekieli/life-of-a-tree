﻿using UnityEngine;
using System.Collections;

public class Seed : MonoBehaviour {

	public bool Mainroot = true;
	Stem stem;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/** Instantiates a stem-prefab from the seed. 
	 * Some species shoot only one mainroot
	 * Others can shoot multiple roots straight from the seed. */
	public void ShootRoot () {
		Stem stem = Instantiate (Resources.Load ("Stem") as Stem);
	}
}
